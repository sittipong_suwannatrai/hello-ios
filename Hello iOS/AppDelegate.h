//
//  AppDelegate.h
//  Hello iOS
//
//  Created by TheYosemite on 9/14/2558 BE.
//  Copyright (c) 2558 IT KMITL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

