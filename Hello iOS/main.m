//
//  main.m
//  Hello iOS
//
//  Created by TheYosemite on 9/14/2558 BE.
//  Copyright (c) 2558 IT KMITL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
