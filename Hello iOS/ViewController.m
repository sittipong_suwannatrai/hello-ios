//
//  ViewController.m
//  Hello iOS
//
//  Created by TheYosemite on 9/14/2558 BE.
//  Copyright (c) 2558 IT KMITL. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *interactiveMessageLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeMessage {
    self.interactiveMessageLabel.text = @"Nice to meet you.";
}

- (void)disableGreetingButton:(UIButton*)sender {
    sender.enabled = NO;
}

- (IBAction)greetingButtonTapped:(id)sender {
    [self changeMessage];
    [self disableGreetingButton:(UIButton*)sender];
}

@end
